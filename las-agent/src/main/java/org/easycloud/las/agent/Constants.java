package org.easycloud.las.agent;

/**
 * Created by IntelliJ IDEA.
 * User: Meng, Ke
 * Date: 13-5-9
 */
public interface Constants {

	static String LOG_AGENT_PROPS = "log-agent.properties";
	static String LOG_AGENT_NAME = "log.agent.name";
	static String LOG_PUSH_PERIOD = "log.push.period";
	static String LOG_PUSH_BEFORE = "log.push.before";
	static String LOG_ROOT_PATH = "log.root.path";
	static String LOG_FILE_PATTERN = "log.file.pattern";
	static String LOG_FILE_POSTFIX = "log.file.postfix";
	static String AVRO_SOURCE_HOST = "avro.source.host";
	static String AVRO_SOURCE_PORT = "avro.source.port";

	static String LOG_CLEAR_ENABLED = "log.clear.enabled";
	static String LOG_CLEAR_PERIOD = "log.clear.period";
	static String LOG_CLEAR_BEFORE = "log.clear.before";
	static String LOG_DIRECTORY_PATTERN = "log.directory.pattern";

	static String LOG_PUSH_PROPS = "log-push.properties";
	static String LAST_PUSHED = "last.pushed";

}
